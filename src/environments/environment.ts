// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: 'AIzaSyBhuPXgKEvBrFEHUcP5PZZ6lZT8fMrcax8',
  authDomain: 'seniors-c2e13.firebaseapp.com',
  databaseURL: 'https://seniors-c2e13.firebaseio.com',
  projectId: 'seniors-c2e13',
  storageBucket: 'seniors-c2e13.appspot.com',
  messagingSenderId: '501848451666'
}
};
