import { Routes, RouterModule } from '@angular/router';
// import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from './services/auth-guard.service';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './popups/login/login.component';


const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent },
  {path: 'dashboard', canActivate: [AuthGuard], component: DashboardComponent
  }
];

export const AppRoutes = RouterModule.forRoot(appRoutes);
