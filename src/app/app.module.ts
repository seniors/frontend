import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireAuthModule} from 'angularfire2/auth';
import { DashboardComponent } from './dashboard/dashboard.component';
import {AuthService} from './services/auth.service';
import {AppRoutes} from './app.routes';
import {AuthGuard} from './services/auth-guard.service';
import {MessagingService} from './services/messaging.service';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {firebaseConfig} from '../environments/firebase.config';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './popups/register/register.component';
import {LoginComponent} from './popups/login/login.component';
import { NotificationsComponent } from './popups/notifications/notifications.component';
import {StepsModule} from 'primeng/steps';
import {DialogModule} from 'primeng/dialog';
import {OverlayPanelModule} from 'primeng/overlaypanel';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    NotificationsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutes,
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireDatabaseModule,
    StepsModule,
    DialogModule,
    OverlayPanelModule
  ],
  // entryComponents: [LoginComponent, RegisterComponent],
  providers: [AuthService, AuthGuard, MessagingService],
  bootstrap: [AppComponent],
})
export class AppModule { }
