import {Component, OnInit} from '@angular/core';
import {MessagingService} from './services/messaging.service';
import * as firebase from 'firebase';
// import { MessagingService } from './messaging.service';
// import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit {
  title = 'app';
  // message: any
  constructor(private msgService: MessagingService) {
    // this.message = firebase.messaging();
  }

  ngOnInit() {
    // this.msgService.getPermission();
    // this.msgService.receiveMessage();
    // this.message = this.msgService.currentMessage;
  }
}
